<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/index.css',
        'css/site.css',
    ];
    public $js = [
        'js/auth.js',
        'js/helpers.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        '\frontend\assets\RtlAsset',
        '\frontend\assets\IconAsset'
    ];
}
